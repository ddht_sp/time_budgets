# -*- coding: utf-8 -*-
"""
Created on Thu May 19 10:36:21 2022

@author: adria036
"""


#%% define functions for areas in the barn

import pandas as pd
import numpy as np

# at the feeding rack
def feeding_rack(X,y):
    """
    A cow is considered to be at the feeding rack when her y-position is 
    larger than -4.6m
    """
    
    # find measurements where cow is at the feeding rack
    feedrackx = X.loc[(y >= -4.6)]
    feedracky = y.loc[(y >= -4.6)]
    
    # combine in dataframe
    feed = pd.DataFrame({'X' : feedrackx,
                             'y' : feedracky})
    
    # calculate percentages
    pfeed = len(feed)/len(y)*100
    
    return feed, pfeed

# at the cubicles next to the exit
def cubicles_A(X,y):
    """
    A cow is considered to be at the cublicles next to the exits and concentrate 
    feeder when her head (the tag) is at least for 1.1m in the cublicle area.
    Concretely, this means:
        - y value <= -16
        - x value between 2.2 and 11 (barn 72)
               or between 23.7 and 32 (barn 70)
    
    """
    # find measurements where cow is in cubicles A
    cub_ax = X.loc[(y <= -16) &  \
                   (((X > 2.2) & (X <= 11)) | \
                   ((X >= 23.7) & (X <= 32)))]
    cub_ay = y.loc[(y <= -16) &  \
                   (((X >= 2.2) & (X <= 11)) | \
                   ((X >= 23.7) & (X <= 32)))]    
    
    # combine in dataframe
    cub_a = pd.DataFrame({'X' : cub_ax,
                          'y' : cub_ay})
    
    # calculate percentages
    pcub_a =  len(cub_a)/len(y)*100
    
    return cub_a,pcub_a

def cubicles_B(X,y):
    """
    A cow is considered to be at the cublicles in the middle of the barn and 
    closest to the exits and concentrate feeder when her head (the tag) is at 
    least for 1.1m in the cublicle area.
    Concretely, this means:
        - y value >= -11.4 and <= -10
        - x value between 0 and 5.7 (barn 72)
               or between 21 and 27 (barn 70)
    NOTE: on plan, not same size /length of cubicle area
    """
    
    # find measurements where cow is in cubicles B
    cub_bx = X.loc[((y >= -11.4) &  (y <= -10 )) & \
                   (((X >= 0) & (X <= 5.7)) | \
                   ((X >= 21) & (X <= 27)))]
    cub_by = y.loc[((y >= -11.4) &  (y <= -10 )) & \
                   (((X >= 0) & (X <= 5.7)) | \
                   ((X >= 21) & (X <= 27)))]
    # combine in dataframe
    cub_b = pd.DataFrame({'X' : cub_bx,
                          'y' : cub_by})
    
    # calculate percentages
    pcub_b =  len(cub_b)/len(y)*100
    
    return cub_b,pcub_b

def cubicles_C(X,y):
    """
    A cow is considered to be at the cublicles in the middle of the barn and 
    farthest from the exits (closest to feed rack) when her head (the tag) is at 
    least for 1.1m in the cublicle area.
    Concretely, this means:
        - y value >= -10 and <= -8.6
        - x value between 0 and 5.7 (barn 72)
               or between 21 and 27 (barn 70)
    NOTE: on plan, not same size /length of cubicle area
    """
    
    # find measurements where cow is in cubicles B
    cub_cx = X.loc[((y > -10) &  (y <= -8.6 )) & \
                   (((X >= 0) & (X <= 5.7)) | \
                   ((X >= 21) & (X <= 27)))]
    cub_cy = y.loc[((y > -10) &  (y <= -8.6 )) & \
                   (((X >= 0) & (X <= 5.7)) | \
                   ((X >= 21) & (X <= 27)))]
    # combine in dataframe
    cub_c = pd.DataFrame({'X' : cub_cx,
                          'y' : cub_cy})
    
    # calculate percentages
    pcub_c =  len(cub_c)/len(y)*100
    
    return cub_c,pcub_c

def cubicles_all(X,y):
    """
    Combined of lying behaviour in area A, B and C
    """
    # in area A
    cub_a, pcub_a = cubicles_A(X,y)
    # in area B
    cub_b, pcub_b = cubicles_B(X,y)
    # in area C
    cub_c, pcub_c = cubicles_C(X,y)
    
    # combine percentages
    pcub = pcub_a+pcub_b+pcub_c
    
    # combine in A, B, C
    cub = pd.concat([cub_a,cub_b, cub_c])

    return cub,pcub

def drink_trough(X,y):
    """
    A cow is considered to be at the cublicles in the middle of the barn and 
    farthest from the exits (closest to feed rack) when her head (the tag) is at 
    least for 1.1m in the cublicle area.
    Concretely, this means:
        - y value >= -10 and <= -8.6
        - x value between 0 and 5.7 (barn 72)
               or between 21 and 27 (barn 70)
    NOTE: on plan, not same size /length of cubicle area
    """
    # find measurements where cow is near drinking trough
    drinkx = X.loc[((y >= -11) &  (y <= -8.9)) & \
                   (((X >= 8.8) & (X <= 10)) | \
                   ((X >= 30.1) & (X <= 32)))]
    drinky = y.loc[((y >= -11) &  (y <= -8.9)) & \
                   (((X >= 8.8) & (X <= 10)) | \
                   ((X >= 30.1) & (X <= 32)))]
    
        # combine in dataframe
    drink = pd.DataFrame({'X' : drinkx,
                          'y' : drinky})
    
    # calculate percentages
    pdrink =  len(drink)/len(y)*100
    
    return drink, pdrink

def conc_feeder(X,y):
    """
    A cow is considered to be at the concentrate feeder when her head (the tag) 
    is at least for 1m in the feeder area.
    Concretely, this means:
        - y value <= -16
        - x value between 1 and 2.2 (barn 72)
               or between 22.3 and 23.7 (barn 70)
    
    """
    # find measurements where cow is in concentrate feeder
    concx = X.loc[(y <= -16) &  \
                   (((X >= 1) & (X < 2.2)) | \
                   ((X >= 22.3) & (X <= 23.7)))]
    concy = y.loc[(y <= -16) &  \
                   (((X >= 1) & (X < 2.2)) | \
                   ((X >= 22.3) & (X <= 23.7)))]    
    
    # combine in dataframe
    conc = pd.DataFrame({'X' : concx,
                         'y' : concy})
    
    # calculate percentages
    pconc =  len(conc)/len(y)*100
    
    return conc,pconc


def assign_behaviour(X,y):
    """
    function to combine all behaviors/locations in 1
    1 = cublicle_A
    2 = cublicle_B
    3 = cublicle_C
    4 = feeding_rack
    5 = drinking_trough
    6 = concentrate_feeder
    0 = no specific area assigned
    """
    
    # calculate time budgets and add the correct number
    cub_a, pcub_a = cubicles_A(X,y)  # in area A
    cub_b, pcub_b = cubicles_B(X,y)  # in area B
    cub_c, pcub_c = cubicles_C(X,y)  # in area C
    drink, pdrink = drink_trough(X,y) # at drinking trough
    feed, pfeed = feeding_rack(X, y)  # at feeding rack
    conc, pconc = conc_feeder(X,y)  # at concentrate feeder
    
    # add numbers
    cub_a["no"] = 1
    cub_b["no"] = 2
    cub_c["no"] = 3
    feed["no"] = 4
    drink["no"] = 5
    conc["no"] = 6
    
    # combine as one dataframe
    data = pd.concat([cub_a,cub_b, cub_c,feed,drink,conc])
    pcombi = pcub_a+pcub_b+pcub_c+pfeed+pdrink+pconc
    
    # indices are retained, use this to merge & find "vain" behaviour
    new = pd.DataFrame({'X' : X,
                        'y' : y,
                        'no' : np.zeros((len(X),), dtype=int)})
    new = new.loc[~new.index.isin(data.index),:]
    
    # combine in 1 data array
    data = pd.concat([data,new])
    data = data.sort_index()
    
 
    return data, pcombi
