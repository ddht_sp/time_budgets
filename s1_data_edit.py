# -*- coding: utf-8 -*-
"""
Created on Wed May 18 15:53:00 2022

@author: adria036

------------------------------------------------------------------------------
KB sensing potential:
    
Module 1:
    - load packages
    - read data
    - edit data - based on 1 measurement per second
    - summarize data
    
"""

#%% import packages

import os
import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt
from datetime import date
from read_data_sewio import read_all_data, read_ids, combine_data_id
from scipy.signal import medfilt
import scipy
import timeit
from filter_data import non_uniform_savgol


#%% functions

def filter_barn_edges(ts,edge1,edge2):
    """
    Filters the data according to barn edges given by edge1 and edge2
    Method: puts data outside edges to nan
        if ts < edge1 : ts = nan
        if ts > edge2 : ts = nan
    NOTE: in a later stage, we can decide to put ts  = closest edge

    Parameters
    ----------
    ts : TYPE = Pandas series (time series)
        DESCRIPTION.
    edge1 : TYPE
        DESCRIPTION.
    edge2 : TYPE
        DESCRIPTION.

    Returns
    -------
    ts_filt : TYPE = Pandas series
        Original series filtered for measurements outside the edges

    """
    #--------------------------------------------------------------------------
    # for development only (delete)
        #ts = copy.copy(data["X"])
        #edge1 = 0
        #edge2 = 32
    #--------------------------------------------------------------------------
    
    # correction1: if more than 30 cm out = nan
    ts.loc[ts <= edge1+0.3] = np.nan
    ts.loc[ts >= edge2+0.3] = np.nan    
    
    # correction2: if less than 30 outside = edge
    ts.loc[(ts > edge1-0.3) & (ts < edge1)] = edge1
    ts.loc[(ts < edge2+0.3) & (ts > edge2)] = edge2
    
    
    return ts


def filter_interp(t,x,y,win_med):
    """
    Filters and interpolates the (x,y) coordinates:
        1) median filter
        2) linear interpolation of the (x,y)-values
    !! apply this function on pieces of data with gaps < 30s for it to be sensible    

    Parameters
    ----------
    t : TYPE = Pandas series
        Contains the time stamp, expressed in seconds
    x : TYPE = Pandas series
        Contains the x coordinate, expressed in meters
    y : TYPE = Pandas series
        Contains the y coordinate, expressed in meters
    win_med : TYPE = Int
        Window size for median filter

    Returns
    -------
    tnew :
        interpolated time array (all seconds, no gaps)
    xfilt : 
        median filtered x data, size = size x
    yfilt :
        median filtered y data, size = size y
    xfilt_new :
        interpolated x data, size = tnew
    yfilt_new :
        interpolated y data, size = tnew
    """
    #--------------------------------------------------------------------------
    # for development only (delete)
    x = data["X"].loc[5000:5400] 
    t = data["relsec"].loc[5000:5400]
    y = data["y"].loc[5000:5400]
    win_med = 31
    #--------------------------------------------------------------------------
    
    # medan filtered x and y
    xfilt = medfilt(x,kernel_size = win_med)
    yfilt = medfilt(y,kernel_size = win_med)
    
    # set values to interpolate
    tmin = t.min()
    tmax = t.max()
    tnew = np.linspace(tmin,tmax,tmax-tmin).round()
    
    # do the interpolation
    f = scipy.interpolate.interp1d(t,[xfilt,yfilt],kind="linear")
    xnew,ynew = f(tnew)
    
    fig = plt.subplots(nrows = 2, ncols = 1,figsize = (25,10))
    plt.subplot(2,1,1)
    plt.xlabel("numeric time [s]")
    plt.ylabel("X [m]")
    plt.plot(t,x,'o-', lw = 3,color = 'mediumblue')
    plt.plot(tnew,xnew,ls = "-",marker = 's',color = 'seagreen')
    plt.plot(t,xfilt,ls = ':', marker = '*',lw=2, color = 'orangered')
    plt.subplot(2,1,2)
    plt.plot(t,y,'o-',lw = 3,color = 'mediumblue')
    plt.plot(tnew,ynew,ls = "-",marker = 's',color = 'seagreen')
    plt.plot(t,yfilt,ls = ':', marker = '*',lw=2, color = 'orangered')
    plt.xlabel("numeric time [s]")
    plt.ylabel("y [m]")
    
    
    return tnew, xfilt, yfilt, xfilt_new, yfilt_new

def filter_interp(t,x,y,win_med,gap_thres):
    
    
    #--------------------------------------------------------------------------
    # for development only (delete)
    x = copy.copy(data["X"].loc[500:2500]).reset_index(drop=1)
    t = copy.copy(data["relsec"].loc[500:2500]).reset_index(drop=1)
    y = copy.copy(data["y"].loc[500:2500]).reset_index(drop=1)
    win_med = 31
    gap_thres = 180
    #--------------------------------------------------------------------------
    # construct dataframe
    df = pd.DataFrame({'X' : x,
                       'y' : y,
                       't' : t})
    # calculate gaps from t
    df["gap"] = t.diff()
    
    # find gapsize < window/2 seconds -- only apply median filter to these
    gapind = df["gap"].loc[df["gap"]>round(win_med/2)].index
    gapind = pd.DataFrame(np.append(gapind,0))
    gapind = gapind.sort_values(by=0).reset_index(drop=1)
    gapind2 = pd.DataFrame(np.append(df["gap"].loc[df["gap"]>round(win_med/2)].index,len(df)-1))
    gapind2 = gapind2.sort_values(by=0).reset_index(drop=1)
    
    # prepare calculations
    df["xfilt"] = np.nan
    df["yfilt"] = np.nan
    
    # calculate median depending on gapsize
    for i in range(0,len(gapind)):
        print(gapind.iloc[i,0],gapind2.iloc[i,0])
        if gapind2.iloc[i,0]-gapind.iloc[i,0] > win_med:
            # median filter
            a = medfilt(df["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=win_med)
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"xfilt"] = a
            a = medfilt(df["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=win_med)
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"yfilt"] = a
        else:
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"xfilt"] = \
                df["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"yfilt"] = \
                df["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
     
    # linear (x,y) interpolation based on xfilt and yfilt
    tmin = df["t"].min()
    tmax = df["t"].max()
    tnew = np.linspace(tmin,tmax,tmax-tmin).round()
    
    # do the interpolation
    f = scipy.interpolate.interp1d(df["t"],[df["xfilt"],df["yfilt"]],kind="linear")
    xnew,ynew = f(tnew)
    
    # to dataframe
    df2 = pd.DataFrame({'t' : tnew,
                        'xnew' : xnew,
                        'ynew' : ynew})
    
    # merge with df based on key = t and tnew
    df3 = pd.merge(df2,df,
                   how = 'outer',
                   on = 't')
                  
    
    # find gapsize > gap_thres seconds -- to delete data
    gapind = df["gap"].loc[df["gap"]>gap_thres]
    gapind2 = df["gap"].loc[df["gap"]>gap_thres]
    gaps = pd.DataFrame({'indx' : gapind,
                          'gapsize' : gapind2}).reset_index(drop=1)
        
    #TODO: if gapsize > gap_thres: replace interpolated data by nan
    # find out how to capture index of gaps
    for i in range(0,len(gaps)):
        print(i)
        try:
            df3.loc[gaps["indx"][i]-gaps["gapsize"][i]:gaps.indx[i]+1:["xnew","ynew"]] = np.nan
    
    #!!!
    
    
    
    fig = plt.subplots(nrows = 2, ncols = 1,figsize = (25,10))
    plt.subplot(2,1,1)
    plt.xlabel("numeric time [s]")
    plt.ylabel("X [m]")
    plt.plot(df["t"],df["X"],'o-', lw = 3,color = 'mediumblue')
    plt.plot(tnew,xnew,ls = "-",lw = 3,color = 'seagreen')
    plt.plot(df["t"],df["xfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    plt.subplot(2,1,2)
    plt.plot(df["t"],df["y"],'o-',lw = 3,color = 'mediumblue')
    plt.plot(tnew,ynew,ls = "-",lw = 3,color = 'seagreen')
    plt.plot(df["t"],df["yfilt"],ls = ':', marker = '*',lw=2, color = 'orangered')
    plt.xlabel("numeric time [s]")
    plt.ylabel("y [m]")
                
    
    
    # clear memory
    del gapind,gapind2,a

    return df["xfilt"],df["yfilt"]



test["xfilt"] = np.nan
test["yfilt"] = np.nan
intdatax = []
intdatay = []
relsec = []
for i in range(0,len(gapind)):
    print(gapind.iloc[i,0],gapind2.iloc[i,0])
    if gapind2.iloc[i,0]-gapind.iloc[i,0] > 31:
        # median filter
        a = medfilt(test["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=31)
        test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = a
        a = medfilt(test["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=31)
        test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = a
        # interpolate on median value
        xmin = test["relsec"].loc[gapind.iloc[i,0]]
        xmax = test["relsec"].loc[gapind2.iloc[i,0]]
        xi = test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        x = np.linspace(xmin, xmax, xmax-xmin)
        # for x
        yi = test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatax = np.append(intdatax,y_nearest)
        # for y
        yi = test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatay = np.append(intdatay,y_nearest)
    else:
        test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = \
            test["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = \
            test["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        
        # interpolate on median value
        xmin = test["relsec"].loc[gapind.iloc[i,0]]
        xmax = test["relsec"].loc[gapind2.iloc[i,0]]
        xi = test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        x = np.linspace(xmin, xmax, xmax-xmin)
        # for x
        yi = test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatax = np.append(intdatax,y_nearest)
        # for y
        yi = test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatay = np.append(intdatay,y_nearest)
               
        relsec = np.append(relsec,x)



#%% set filepaths and constants


# set path to data X,y
path_os = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio"
    )

# set start and end dates
startdate = date(2022,5,1)
enddate = date(2022,5,2)

# set path to excel file with cowids
path = os.path.join("W:","/ASG","WLR_Dataopslag","DairyCampus",
                    "3406_Nlas","raw","dc_sewio"
                    )

# set file name for cowid information
fn = "\Copy_of_Sewio_tags.xlsx"


#%% load data and edit for number of seconds data retained = nsec

# read position data
data = read_all_data(startdate,enddate,path_os)

# read id/tag data
tags = read_ids(path, fn)

# combine it with data
data = combine_data_id(data,tags)
data = data[["cowid","barn","date","at","feed_reference","title","X","y","alias"]]

# unique 'seconds' in the dataset 
data["day"] = data["at"].dt.day - min(data["at"].dt.day)
data["hour"] = data["at"].dt.hour
data["minute"] = data["at"].dt.minute
data["second"] = data["at"].dt.second
data["relsec"] = data["day"]*86400 \
                 + data["hour"]*3600 \
                 + data["minute"]*60 \
                 + data["second"]
nsec = data["relsec"]
cowids = data["cowid"]
df = pd.concat([cowids,nsec],axis = 1).drop_duplicates()  # gives the indices of unique first 
del df["relsec"], df["cowid"]

# innerjoin for selection step
data = pd.concat([data,df],axis = 1, join = 'inner')
data = data.sort_values(by = ["cowid","at"]).reset_index(drop=1)

# add relative time expressed in days
data["reltime"] = data["day"] + (data["hour"]*3600 +
                                 data["minute"]*60 +
                                 data["second"])/86400

# calculate gaps based on numeric time relative to start of dataset
data["gap"] = data["relsec"].diff()
data.loc[data["gap"] < 0,"gap"] = np.nan

# set data outside edges to edges x < 0 and < 10.6 or x < 21 and x > 32
data.loc[(data["X"] < 15),"X"] = filter_barn_edges( \
              copy.copy(data.loc[(data["X"] < 15),"X"]),0,10.6) #barn 72
data.loc[(data["X"] >= 15),"X"] = filter_barn_edges( \
              copy.copy(data.loc[(data["X"] >= 15),"X"]),21,32) # barn 70
# set data outside edges to edges y < -18 or y > -2.5
data["y"] = filter_barn_edges(copy.copy(data["y"]),-18,-2.5)
data["X"].hist()
data["y"].hist()

# clear workspace
del df, enddate, startdate, path, path_os, fn, tags, nsec, cowids 


#%% visualise and quality check
"""
    - amount of data points without the cowid/tag linked (OK)
    - gapsize
    - summary stats per animal + missing data visualisations
    - to be added
"""

# data for which no cowid is available = 0
nocow = data.loc[data["cowid"]==0 & data["X"].isnull(),:]
nocow1 = data.loc[data["cowid"]==0,:]
print("Number of datapoints without cowid and equal to NaN = " + str(len(nocow)))
print("This is " + str(len(nocow)/len(nocow1)*100) + "% of the records with no cowid")

# delete these rows
data = data.loc[data["cowid"]>0,:]
del nocow, nocow1

# cows in the barn
cows = data["cowid"].drop_duplicates().sort_values().reset_index(drop=1)
cows = pd.DataFrame(cows)

# cowdays in the barn
cowday = data[["cowid","date"]].drop_duplicates().sort_values(by = ["cowid","date"]).reset_index(drop=1)

#TODO: need to check what to do when for a cow data are not available --> ignore?

#TODO: filtering!! --> median filter? for extrapolations

# summarize gaps and missing data (and relate this to the location if possible)



# TODO: gap visualisations before filtering/data imputation > most gaps 1-5 seconds
# TODO: FIRST median smooth X and Y separately

# TODO: THEN data imputation with Savitsky-Golay function previously implemented
data["gap"].hist()
data.loc[(data["gap"] < 60) & (data["gap"] > 1),"gap"].hist()
data.loc[(data["gap"] < 60) & (data["gap"] > 5),"gap"].hist()

# number of datapoints and missing data (for selection purposes)
counts = data.groupby(by = ["cowid","date"]).count()
cowday["count"] = counts["barn"]


cows[""]


#%% filter the data


test = data.loc[(data["cowid"] == 427) & (np.floor(data["reltime"]) == 1),:]
test = test.reset_index(drop=1)

# find gapsize < 60 seconds -- only apply median filter to these
gapind = test.loc[test["gap"]>60].index
gapind = pd.DataFrame(np.append(gapind,0))
gapind = gapind.sort_values(by=0).reset_index(drop=1)
gapind2 = pd.DataFrame(np.append(test.loc[test["gap"]>60].index,len(test)-1))
gapind2 = gapind2.sort_values(by=0).reset_index(drop=1)

test["xfilt"] = np.nan
test["yfilt"] = np.nan
intdatax = []
intdatay = []
relsec = []
for i in range(0,len(gapind)):
    print(gapind.iloc[i,0],gapind2.iloc[i,0])
    if gapind2.iloc[i,0]-gapind.iloc[i,0] > 31:
        # median filter
        a = medfilt(test["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=31)
        test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = a
        a = medfilt(test["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=31)
        test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = a
        # interpolate on median value
        xmin = test["relsec"].loc[gapind.iloc[i,0]]
        xmax = test["relsec"].loc[gapind2.iloc[i,0]]
        xi = test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        x = np.linspace(xmin, xmax, xmax-xmin)
        # for x
        yi = test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatax = np.append(intdatax,y_nearest)
        # for y
        yi = test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatay = np.append(intdatay,y_nearest)
    else:
        test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = \
            test["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]] = \
            test["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        
        # interpolate on median value
        xmin = test["relsec"].loc[gapind.iloc[i,0]]
        xmax = test["relsec"].loc[gapind2.iloc[i,0]]
        xi = test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        x = np.linspace(xmin, xmax, xmax-xmin)
        # for x
        yi = test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatax = np.append(intdatax,y_nearest)
        # for y
        yi = test["yfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        interp = scipy.interpolate.interp1d(xi, yi, kind = "nearest")
        y_nearest = interp(xi)
        intdatay = np.append(intdatay,y_nearest)
               
        relsec = np.append(relsec,x)
            
       



#%timeit test["xfilt"] = medfilt(test["X"],kernel_size = [31])
#%timeit test["yfilt"] = medfilt(test["y"],kernel_size = [31])

# visualise filtered data
fig,ax = plt.subplots(ncols = 1, nrows = 2, figsize = (20,10))
plt.subplot(2,1,1)
plt.plot(test["reltime"],
                 test["X"],
                 color = "indigo",
                 lw = 2)
plt.plot(test["reltime"],
                 test["xfilt"],
                 color = "darkturquoise",
                 ls = "-")
plt.xlabel("relative time")
plt.ylabel("x")

plt.subplot(2,1,2)
ax[1] = plt.plot(test["reltime"],
                 test["y"],
                 color = "indigo",
                 lw = 2)
ax[1] = plt.plot(test["reltime"],
                 test["yfilt"],
                 color = "darkturquoise",
                 ls = "-")
plt.xlabel("relative time")
plt.ylabel("y")

# visualise interpolated data (in relsec, intdatax and intdatay)


# visualise gaps
fig,ax = plt.subplots(ncols = 2, nrows = 1, figsize = (20,10))
plt.subplot(1,2,1)
plt.grid(visible = True,axis = 'both')
plt.hist(test.loc[test["gap"]<600,["gap"]], color = "darkred")
plt.ylim(0, 16000)
plt.xlim(0,600)
plt.xlabel("gapsize [s]")
plt.ylabel("No. of gaps")
plt.title("gaps < 600s")
plt.subplot(1,2,2)
plt.grid(visible = True,axis = 'both')
plt.hist(test.loc[test["gap"]<120,["gap"]], bins = 48,color = "darkred")
plt.plot([31,31],[0,16000], linewidth=2, color = "darkblue")
plt.ylim(0, 15000)
plt.xlim(0,121)
plt.xlabel("gapsize [s]")
plt.ylabel("No. of gaps")
plt.title("gaps < 120s")


fig,ax = plt.subplots(ncols = 1, nrows = 2, figsize = (20,10))
plt.subplot(2,1,1)
plt.plot(test["relsec"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]-2],
         test["xfilt"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]-2])
plt.plot(test["relsec"].loc[gapind.iloc[i,0]+8:gapind2.iloc[i,0]-8],
         ysm[7:-7])
plt.xlabel("relative time [s]")
plt.ylabel("smoothed & filtered x")

plt.subplot(2,1,2)
plt.plot(test["relsec"].loc[gapind.iloc[i,0]+8:gapind2.iloc[i,0]-8],
          ysm[7:-7], lw = 2, color = "orange")
plt.plot(yall["frameno"],
         yall["mean"],
         color = "darkred")
plt.xlabel("relative time [s]")
plt.ylabel("interpolated filtered x")
