# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 11:02:10 2022

@author: adria036
"""

import os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm
# %matplotlib qt

#%% prepare and summarize times series of data for behaviour/area usage


# load data

path_data =  r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\BEC3\data'


# load 
data = pd.DataFrame([])
for f in os.listdir(path_data):
    print(f)
    if ("_bec3.txt" in f) and ("diagnosis" not in f) and ("milk" not in f) and ("treatment" not in f) and ("Custom" not in f) and ("research" not in f) and ("query" not in f):
        new= pd.read_csv(path_data+ "\\" + f)
        new = new.drop(["acc_x","acc_y","acc_z"],axis = 1)
        
        # sort and set nan to nan
        new = new.sort_values(by = ["cowid","date","t"])
        new.loc[(~new["X"].isna()) & (new["xnew"].isna()),"xnew"] = new.loc[(~new["X"].isna()) & (new["xnew"].isna()),"X"]
        new.loc[(~new["y"].isna()) & (new["ynew"].isna()),"ynew"] = new.loc[(~new["y"].isna()) & (new["ynew"].isna()),"y"]
        new.loc[(new["ynew"].isna()),"area"] = np.nan
        
        # check number of records
        number_records = new[["cowid","date","xnew"]].groupby(by=["cowid","date"]).count().reset_index()
        number_records = number_records.rename(columns = {"xnew" : "count"})
        
        # calculate time spent 
        area_budget = new[["cowid","date","area","xnew"]].groupby(by = ["cowid","date","area"]).count().reset_index()
        area_budget = area_budget.rename(columns = {"xnew" : "areacount"})
        area_budget = area_budget.merge(number_records,on =["cowid","date"],how ="outer")
        area_budget["percentage"] = area_budget["areacount"] / area_budget["count"] *100
               
        del new
        
        data = pd.concat([data,area_budget])

# drinking and feeding datasets
drink = data.loc[data["area"] == 5, :]
feed = data.loc[data["area"] == 4,:]
lying = data.loc[data["area"] <= 3,:]
conc = data.loc[data["area"] ==5,:]
vain = data.loc[data["area"] == 0,:]

# datasets save
path = r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\data'
drink.to_csv(path + "\\drink.txt")
feed.to_csv(path + "\\feed.txt")
lying.to_csv(path + "\\lying.txt")
conc.to_csv(path + "\\conc.txt")
vain.to_csv(path + "\\vain.txt")


#%% load data
path = r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\data'

feed = pd.read_csv(path + "\\feed.txt", index_col = 0)
lying = pd.read_csv(path + "\\lying.txt", index_col = 0)
conc = pd.read_csv(path + "\\conc.txt", index_col = 0)
vain = pd.read_csv(path + "\\vain.txt", index_col = 0)
del path

#%% drinking behaviour

# load THI
path = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\General\supervision"
data = pd.read_csv(path+"\\Area_usage_dec2021-aug2022.csv")
data = data[["Date","meanTHI"]].rename(columns={"Date":"date"})
data["date"] = pd.to_datetime(data["date"],format = "%d/%m/%Y")
data = data.drop_duplicates()


# load drinking behaviour
path = r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\data'
drink = pd.read_csv(path + "\\drink.txt", index_col = 0)
drink["date"] = pd.to_datetime(drink["date"],format = "%Y-%m-%d")
drink = drink.sort_values(by = ["cowid","date"]).reset_index(drop=1)
dates = drink["date"].drop_duplicates().sort_values().reset_index(drop=1).reset_index()
drink = drink.merge(dates,how="outer",on = "date")
drink = drink.rename(columns={'index':"t"})
drink = drink.sort_values(by = ["cowid","date"]).reset_index(drop=1)
drink = drink.loc[drink["t"]<236,:]  #♥ remove data after 31/8
dates = dates.merge(data,on = "date",how="inner")
dates["ones"] = np.ones((len(dates),1))*-1
dates["thicor"] =5*((dates["meanTHI"]-min(dates["meanTHI"]))/(max(dates["meanTHI"])-min(dates["meanTHI"])))+3
dates["meanTHI"] = round(dates["meanTHI"]/3)*3
dates["date2"] = dates["date"].dt.strftime("%d/%m")

# number of measurements per cow at least 2 months
smry = drink[["cowid","percentage"]].groupby(by = ["cowid"]).count()  #101 cows
smry = smry.sort_values(by = "percentage").reset_index()
smry = smry.loc[smry["percentage"]>=60,:]   # 61 cows with more than 60 data points
drink = drink.merge(smry["cowid"],how="inner",on="cowid")

# # plot distributiions
# fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
# sns.violinplot(x="cowid",y="percentage",data=drink)

# plot trend of data by date
fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
sns.boxplot(data=drink,x="t",y="percentage",fliersize = 0, whis = 0.8)
sns.lineplot(data=dates,x="index",y="ones",hue = 'meanTHI',linewidth = 40,
             hue_norm = (dates["meanTHI"].min(),dates["meanTHI"].max()),
             palette = cm.cool, legend=False)
sns.lineplot(data=dates,x="index",y="thicor",linewidth = 1,color="r",legend=False)
ax.set_title("distribution of drinking behaviour across individual cows")
labels = dates.iloc[np.linspace(0,max(dates.index),round(max(dates.index)/10)).round()]
labels = labels.date2
plt.xticks(np.linspace(0,max(dates.index),round(max(dates.index)/10)).round(),labels = labels)
ax.set_xlabel("date")
ax.set_ylabel("percentage time spent at drinking trough [%]")
ax.set_ylim([-1.5,9])
plt.savefig(r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\results\\' + 'drinking.png')


#%% walking
# load THI
path = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\General\supervision"
data = pd.read_csv(path+"\\Area_usage_dec2021-aug2022.csv")
data = data[["Date","meanTHI"]].rename(columns={"Date":"date"})
data["date"] = pd.to_datetime(data["date"],format = "%d/%m/%Y")
data = data.drop_duplicates()


# load walk behaviour
path = r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\data'
vain = pd.read_csv(path + "\\vain.txt", index_col = 0)
vain["date"] = pd.to_datetime(vain["date"],format = "%Y-%m-%d")
vain = vain.sort_values(by = ["cowid","date"]).reset_index(drop=1)
dates = vain["date"].drop_duplicates().sort_values().reset_index(drop=1).reset_index()
vain = vain.merge(dates,how="outer",on = "date")
vain = vain.rename(columns={'index':"t"})
vain = vain.sort_values(by = ["cowid","date"]).reset_index(drop=1)
vain = vain.loc[vain["t"]<236,:]  #♥ remove data after 31/8
dates = dates.merge(data,on = "date",how="inner")
dates["ones"] = np.ones((len(dates),1))*-1
dates["thicor"] =20*((dates["meanTHI"]-min(dates["meanTHI"]))/(max(dates["meanTHI"])-min(dates["meanTHI"])))+20
dates["meanTHI"] = round(dates["meanTHI"]/3)*3
dates["date2"] = dates["date"].dt.strftime("%d/%m")

# number of measurements per cow at least 2 months
smry = vain[["cowid","percentage"]].groupby(by = ["cowid"]).count()  #101 cows
smry = smry.sort_values(by = "percentage").reset_index()
smry = smry.loc[smry["percentage"]>=60,:]   # 61 cows with more than 60 data points
vain = vain.merge(smry["cowid"],how="inner",on="cowid")

# # plot distributiions
# fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
# sns.violinplot(x="cowid",y="percentage",data=vain)

# plot trend of data by date
fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
sns.boxplot(data=vain,x="t",y="percentage",fliersize = 0, whis = 0.8)
sns.lineplot(data=dates,x="index",y="ones",hue = 'meanTHI',linewidth = 40,
             hue_norm = (dates["meanTHI"].min(),dates["meanTHI"].max()),
             palette = cm.cool, legend=False)
sns.lineplot(data=dates,x="index",y="thicor",linewidth = 1,color="r",legend=False)
ax.set_title("distribution of walking behaviour across individual cows")
labels = dates.iloc[np.linspace(0,max(dates.index),round(max(dates.index)/10)).round()]
labels = labels.date2
plt.xticks(np.linspace(0,max(dates.index),round(max(dates.index)/10)).round(),labels = labels)
ax.set_xlabel("date")
ax.set_ylabel("percentage time spent at walking [%]")
ax.set_ylim([-1.5,45])
plt.savefig(r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\results\\' + 'walking.png')

#%% resting behaviour

# load THI
path = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\General\supervision"
data = pd.read_csv(path+"\\Area_usage_dec2021-aug2022.csv")
data = data[["Date","meanTHI"]].rename(columns={"Date":"date"})
data["date"] = pd.to_datetime(data["date"],format = "%d/%m/%Y")
data = data.drop_duplicates()


# load walk behaviour
path = r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\data'
ly = pd.read_csv(path + "\\lying.txt", index_col = 0)
ly["date"] = pd.to_datetime(ly["date"],format = "%Y-%m-%d")
ly = ly.sort_values(by = ["cowid","date"]).reset_index(drop=1)
dates = ly["date"].drop_duplicates().sort_values().reset_index(drop=1).reset_index()
ly = ly.merge(dates,how="outer",on = "date")
ly = ly.rename(columns={'index':"t"})
ly = ly.sort_values(by = ["cowid","date"]).reset_index(drop=1)
ly = ly.loc[ly["t"]<236,:]  #♥ remove data after 31/8
dates = dates.merge(data,on = "date",how="inner")
dates["ones"] = np.ones((len(dates),1))*-1
dates["thicor"] =20*((dates["meanTHI"]-min(dates["meanTHI"]))/(max(dates["meanTHI"])-min(dates["meanTHI"])))+40
dates["meanTHI"] = round(dates["meanTHI"]/3)*3
dates["date2"] = dates["date"].dt.strftime("%d/%m")

# number of measurements per cow at least 2 months
smry = ly[["cowid","percentage"]].groupby(by = ["cowid"]).count()  #101 cows
smry = smry.sort_values(by = "percentage").reset_index()
smry = smry.loc[smry["percentage"]>=60,:]   # 61 cows with more than 60 data points
ly = ly.merge(smry["cowid"],how="inner",on="cowid")

# # plot distributiions
# fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
# sns.violinplot(x="cowid",y="percentage",data=ly)

# plot trend of data by date
fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
sns.boxplot(data=ly,x="t",y="percentage",fliersize = 0, whis = 0.8)
sns.lineplot(data=dates,x="index",y="ones",hue = 'meanTHI',linewidth = 40,
             hue_norm = (dates["meanTHI"].min(),dates["meanTHI"].max()),
             palette = cm.cool, legend=False)
sns.lineplot(data=dates,x="index",y="thicor",linewidth = 1,color="r",legend=False)
ax.set_title("distribution of lying behaviour across individual cows")
labels = dates.iloc[np.linspace(0,max(dates.index),round(max(dates.index)/10)).round()]
labels = labels.date2
plt.xticks(np.linspace(0,max(dates.index),round(max(dates.index)/10)).round(),labels = labels)
ax.set_xlabel("date")
ax.set_ylabel("percentage time spent lying [%]")
ax.set_ylim([-1.5,70])
plt.savefig(r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\results\\' + 'lying.png')

#%% individual cow behaviour

%matplotlib qt
# from pyspc import *
import os
import pandas as pd
import numpy as np 
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib
# load THI
path = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\General\supervision"
data = pd.read_csv(path+"\\Area_usage_dec2021-aug2022.csv")
data = data[["Date","meanTHI"]].rename(columns={"Date":"date"})
data["date"] = pd.to_datetime(data["date"],format = "%d/%m/%Y")
data = data.drop_duplicates()

# load drinking behaviour
path = r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\data'
drink = pd.read_csv(path + "\\drink.txt", index_col = 0)
drink["date"] = pd.to_datetime(drink["date"],format = "%Y-%m-%d")
drink = drink.sort_values(by = ["cowid","date"]).reset_index(drop=1)
dates = drink["date"].drop_duplicates().sort_values().reset_index(drop=1).reset_index()
drink = drink.merge(dates,how="outer",on = "date")
drink = drink.rename(columns={'index':"t"})
drink = drink.sort_values(by = ["cowid","date"]).reset_index(drop=1)
drink = drink.loc[drink["t"]<236,:]  #♥ remove data after 31/8
dates = dates.merge(data,on = "date",how="inner")
dates["ones"] = np.ones((len(dates),1))*-1
dates["thicor"] =5*((dates["meanTHI"]-min(dates["meanTHI"]))/(max(dates["meanTHI"])-min(dates["meanTHI"])))+3
dates["meanTHI"] = round(dates["meanTHI"]/3)*3
dates["date2"] = dates["date"].dt.strftime("%d/%m")


# fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))

a = spc(drink.loc[drink["cowid"]==7952,"percentage"].reset_index(drop=1))  + xmr()
print(a)



#%% write statistical process control chart for individual subjects



def mr_cc(data,t):
    
    # initaliaze cc
    d = 1.128
    
    chart = pd.DataFrame(np.zeros((len(data),7)), columns = ["t","data","ucl","cl","lcl","ooc","mr"])
    chart["t"] = t.values
    chart["data"] = data.values
    
    # initalise ccon first 10 measurements  
    mr0 = data.iloc[0:10].diff().abs().mean()  # 
    ucl = data.iloc[0:10].mean() + 3*mr0/d
    lcl = data.iloc[0:10].mean() - 3*mr0/d
    cl = data.iloc[0:10].mean()
    ucl = max(ucl,cl+2) # set minimumwidth of 2%
    lcl = min(ucl,cl-2)
    
    chart["cl"][0:10] = cl
    chart["ucl"][0:10] = ucl
    chart["lcl"][0:10] = lcl
    chart["mr"][0:10] = mr0
    chart["ooc"][0:10] = np.multiply((chart["data"][0:10]<chart["lcl"][0:10]) | (chart["data"][0:10]>chart["ucl"][0:10]),1)
    
    for idx in chart.index.values[10:]:
        #print(idx)
        
        # calculate mr, lcl, ucl, cl with values in control 
        mr = chart.loc[(chart["ooc"] == 0) & (chart.index.values < idx),"data"].diff().abs().mean()  # 
        ucl = chart.loc[(chart["ooc"] == 0) & (chart.index.values < idx),"data"].mean() + 3*mr/d
        lcl = chart.loc[(chart["ooc"] == 0) & (chart.index.values < idx),"data"].mean() - 3*mr/d
        cl = chart.loc[(chart["ooc"] == 0) & (chart.index.values < idx),"data"].mean()
        ucl = max(ucl,cl+2) # set minimumwidth
        lcl = min(ucl,cl-2)
        
        # add to chart and check ooc
        chart["cl"][idx] = cl
        chart["ucl"][idx] = ucl
        chart["lcl"][idx] = lcl
        chart["mr"][idx] = mr
        chart["ooc"][idx] = np.multiply((chart["data"][idx]<chart["lcl"][idx]) | (chart["data"][idx]>chart["ucl"][idx]),1)
        
    return chart
    
chart["labels"] = spc["date"].dt.strftime("%d/%m").values
    
def cc_fig(chart):
    
    fig,ax = plt.subplots(nrows = 1, ncols = 1, figsize=(18,7))
    
    ax.plot(chart["t"],chart["data"],linestyle = ':',marker = "o",
            color = "blue", markersize = 8, linewidth = 2)
    ax.plot(chart["t"],chart["cl"],linestyle = '-',
            color = "k", linewidth = 2)
    ax.plot(chart["t"],chart["lcl"],linestyle = '--',
            color = "teal", linewidth = 2)
    ax.plot(chart["t"],chart["ucl"],linestyle = '--',
            color = "teal", linewidth = 2)   
    ax.fill_between(chart["t"],chart["lcl"],chart["ucl"],color ="teal", alpha = 0.2 )
    ax.plot(chart.loc[chart["ooc"]==1,"t"],chart.loc[chart["ooc"]==1,"data"], marker = "X",
            color = "red",linestyle = '', markersize = 15)
    ax.set_xlabel("time")
    ax.set_ylabel("data")
    ax.set_title("control chart for individual subjects")
    plt.xticks(chart["t"][0::5].values,labels = chart["labels"][0::5].values)
    ax.set_ylim(min(min(chart["data"]),min(chart["lcl"]),0),max(max(chart["data"]),max(chart["lcl"]),10))

    return fig,ax
    

for cow in drink["cowid"].drop_duplicates():
    spc = drink.loc[drink["cowid"]==cow,["date","t","percentage"]]
    chart = mr_cc(spc["percentage"],spc["t"])
    chart["labels"] = spc["date"].dt.strftime("%d/%m").values

    fix,ax = cc_fig(chart)
    plt.savefig(r'C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\ddhtSP\results\\' + 'cc_cow' + str(cow) + '.png')
    plt.close()



